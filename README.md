# Custom CA

A repo for testing custom Certificate Authority support across our analyzers.

## Test Approach

The way that this repo tests for custom CA support is that it builds a test docker image with the analyzer
under test's docker image as the base image. This allows us to inject a small go test binary
[ssl-test.go](./ssl-test.go) so we can run it in the context of the analyzer's docker image. When the tests
are run with this test docker image, it spins up the
<https://gitlab.com/gitlab-org/security-products/tests/custom-ca-ssl-server> docker image as a service to hit,
then sets the `ADDITIONAL_CA_CERT_BUNDLE` env var value to the CA cert value used to sign the cert used in
that service.

## CI Approach

The CI configuration will run tests against all analyzers. Each analyzer has a trigger to run these tests
targeting the specific analyzer with something like the following which targets the gosec analyzer test.

```yaml
test-custom-ca-bundle:
  variables:
    BASE_IMAGE: $CI_REGISTRY_IMAGE/tmp:$CI_COMMIT_SHA
    TARGET: test-gosec
  trigger:
    project: "gitlab-org/security-products/tests/custom-ca"
    branch: master
    strategy: depend
```
